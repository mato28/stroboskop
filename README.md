# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:
```
git clone https://mato28@bitbucket.org/mato28/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/mato28/stroboskop/commits/0240d0666646b25e17c4d9f7d2e2ccc2fc7ad3e5?at=master

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/mato28/stroboskop/commits/475a8b4c5a2f56557bab98fc03967f60537d2e97

Naloga 6.3.2:
https://bitbucket.org/mato28/stroboskop/commits/10e0ce3110291ed93cb704d873cd28960ec951f7

Naloga 6.3.3:
https://bitbucket.org/mato28/stroboskop/commits/1fb7e50e3d8c943a21e014e04b914282971f296a

Naloga 6.3.4:
https://bitbucket.org/mato28/stroboskop/commits/4a579065aef9683207c00e029708548d9e10ccb5

Naloga 6.3.5:
```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/mato28/stroboskop/commits/9b5a1dd5e0ea7af6304646bce30838507521fc40?at=dinamika

Naloga 6.4.2:
https://bitbucket.org/mato28/stroboskop/commits/7680015b6c62ddfa151c633cd5f3d6fb9c0985f6?at=dinamika

Naloga 6.4.3:
https://bitbucket.org/mato28/stroboskop/commits/667e4513d26086fd1215e8533d1eb8acdf0e5407?at=master

Naloga 6.4.4:
https://bitbucket.org/mato28/stroboskop/commits/667e4513d26086fd1215e8533d1eb8acdf0e5407?at=master